package com.om.example.loginservice;

public interface IAccount {
	   void setLoggedIn(boolean value);
	   boolean passwordMatches(String candidate);
	   void setRevoked(boolean value);
	   boolean isLoggedIn();
	   boolean isRevoked();
	   boolean isPasswordExpired();
	   boolean isPasswordTemporary();
	   boolean isPasswordChanged();
}
